
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*


- [정의](#정의)
- [STRONG_POINT_OF_CLOUD_SERVICE](#STRONG_POINT_OF_CLOUD_SERVICE)
- [단점](#단점)



<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## 정의
    - 클라우드(인터넷)을 통해 가상화된 컴퓨터의 시스템리소스(IT 리소스)를 제공하는 것이다.    
<img src="market_share.jpeg" width="640" height="360" />
    - https://www.youtube.com/watch?v=YQAMjXVlb9Y


## STRONG_POINT_OF_CLOUD_SERVICE
    - 이용자가 많을 수록 스케일링이 쉬움
    - 기존에는 인프라 엔지닝가 필요했던 다양한 부가 기능을 제공함
    - DevOps 엔지니어의 대두!
        - 앱의 테스트, 배포, 운영을 담당하는 엔지니어
    - 서버의 하드웨어적인 부분을 직접 관리하지 않아도 됨
    - 보안이 우수 (AWS)
    

## 단점
    - AWS 서버도 가끔은 잠을 잔다.
        - 2019년 초 AWS 서버가 죽어서, 많은 스타트업 백엔드 개발자들이 고통을 겪은 바 있음
    - Seoul Region은 업데이트 속도가 다른 지역에 비해 느림

