
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*


- [MySQL](#MySQL)
- [Install](#Install)
- [Establish_Connection](#Establish_Connection)
- [Pooling_Connections](#Pooling_Connections)
- [SELECT](#SELECT)
- [JOIN](#JOIN)
- [INSERT](#INSERT)
- [UPDATE](#UPDATE)
- [DELETE](#DELETE)
- [Query_forms](#Query_forms)
- [Inection_Attack](#Inection_Attack)
- [Escaping_Inection_Attack](#Escaping_Inection_Attack)
- [Transaction_And_Rollback](#Transaction_And_Rollback)



<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## MySQL
    - 오픈 소스의 관계형 데이터베이스 관리 시스템(RDBMS)
    - 관계형 데이터베이스 : 관계화 된 모델이 따라 데이터들을 체계적으로 저장 · 관리하기 위한 시스템
    - 관계형 모델 : 데이터를 2차원 구조로 표현
| index | name | address | type |
|:--------:|:--------:|:--------:|:--------:|
| 0 | 강남초등학교 | 강남구 | 초등학교 |
| 1 | 신구초등학교 | 강남구 | 초등학교 |


## Install
    -  npm install --save mysql


## Establish_Connection
```javascript
    var mysql      = require('mysql');
    var connection = mysql.createConnection({
    host     : 'example.org',
    user     : 'bob',
    password : 'secret'
    });

    connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
    });
```
    - 위의 방식이 권장 되는 방식이나, 쿼리 명령어를 실행하면서 선언할 수도 있습니다.
```javascript
    var mysql      = require('mysql');
    var connection = mysql.createConnection(...);

    connection.query('SELECT 1', function (error, results, fields) {
    if (error) throw error;
    // connected!
    });
```    
    - host: 연결하는 데이터 베이스의 주소
    - port: 연결하고자 하는 포트 넘버 (Default: 3306)    
    - user: 인증 하고자 하는 mysql user
    - password: mysql password
    - database: 데이터베이스 이름 (Optional).    
    - timezone: 적용 되는 time zone (한국, 미국 등)
    

## Pooling_Connections
    - 일일히 connection을 열고 닫기 보다는 pooling 이라는 개념을 사용하기도 합니다.
    - 데이터베이스로의 추가 요청이 필요할 때 연결을 재사용할 수 있도록 관리되는 데이터베이스 연결의 캐시입니다.
    - 연결 풀을 사용하면 데이터베이스의 명령 실행의 성능을 강화할 수 있다.
        - 각 사용자마다 데이터베이스 연결을 열고 유지보수하는 것은 비용이 많이 들고 자원을 낭비하는데,
        - 매 번 연결을 열고 닫는 게 아니기 때문
    - 연결 풀의 경우 연결이 수립된 이후에 풀에 위치해 있으므로 다시 사용하면 새로운 연결을 수립할 필요가 없어진다.     
```javascript
    var mysql = require('mysql');
    var pool  = mysql.createPool({
        connectionLimit : 10,
        host            : 'example.org',
        user            : 'bob',
        password        : 'secret',
        database        : 'my_db'
    });

    pool.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
        if (error) throw error;
        console.log('The solution is: ', results[0].solution);
    });
```  
    - 위의 경우, query로 명령을 할 때에 새로운 connection을 가져올 수 있으니 아래와 같이 작업 하는 것을 권장합니다.
```javascript
    var mysql = require('mysql');
    var pool  = mysql.createPool(...);

    pool.getConnection(function(err, connection) {
    if (err) throw err; // not connected!

        // Use the connection
        connection.query('SELECT something FROM sometable', function (error, results, fields) {
            // When done with the connection, release it.
            connection.release();

            // Handle error after the release.
            if (error) throw error;

            // Don't use the connection here, it has been returned to the pool.
        });
    });
});
```  


## SELECT
    - SELECT * FROM schools
        - schools 데이터베이스에서 모든 데이터를 불러옵니다.
    - SELECT name, address FROM schools
        - 데이터베이스에서 원하는 항목만 불러온다.
        - 유저 데이터 정보를 보낼 시, 비밀번호 등 응답하지 않아야 하는 정보가 있을 시 위와 같이 처리합니다.
    - SELECT * FROM schools WHERE school = 'awesome'
        - 특정 조건에 맞추어서 불러 옵니다.
    - SELECT * FROM schools WHERE school = 'awesome' LIMIT 10, 10
        - 데이터 항목을 순서로 잘라서 불러 옵니다.
        - 주로 게시판의 페이징 등을 구현할 때 활용합니다.    
        - LIMIT 10 : 처음부터 10개만 불러옵니다.
        - LIMIT 9, 10 : 10번째 줄로부터 10개의 데이터를 불러 옵니다. (첫번째 줄은 0부터 시작합니다)
    - SELECT * FROM schools WHERE school = 'awesome' ORDER BY name LIMIT 10, 20 
        - 데이터를 불러올 때 ORDER BY라는 항목 뒤의 컬럼에 따라 불러옵니다.
        - ORDER BY name (ASC) : 오름차순 (ASC는 생략 가능)
        - ORDER BY name DESC : 내림차순


## JOIN
```javascript
    //schools data
    [
        { idx: 1, name: 'School1', address:"gangman"},
        { idx: 2, name: 'School2', address:"gangman"},
        { idx: 3, name: 'School3', address:"sinchon"},
        { idx: 4, name: 'School4', address:},
        { idx: 5, name: 'School5', address:}
    ]
    //stduents data
    [
        { idx: 1, name: '철수', school:'School1' },
        { idx: 2, name: '영희', school:'School1'},
        { idx: 3, name: '태수', school:'School2' }
        { idx: 4, name: '없음', school: }
    ]
``` 
    - JOIN (Inner Join)
        - 양 조건을 모두 만족하는 데이터를 불러 옵니다.
        - SELECT schools.address AS schoolAddress, students.name AS stduent FROM students JOIN schools ON students.school = schools.name
        - 결과
```javascript
        [
            { "stduent": "철수", "schoolAddress": "gangman" },
            { "stduent": "영희", "schoolAddress": "gangman" },
            { "stduent": "태수", "schoolAddress": "sinchon" }
        ]
```
    - LEFT JOIN
        - 오른쪽 조건에 상관 없이, 왼족 데이터를 기준으로 모두 불러 옵니다.
        - SELECT schools.address AS schoolAddress, students.name AS stduent FROM students LEFT JOIN schools ON students.school = schools.name
        - 결과
```javascript
        [
            { "stduent": "철수", "schoolAddress": "gangman" },
            { "stduent": "영희", "schoolAddress": "gangman" },
            { "stduent": "태수", "schoolAddress": "sinchon" }
            { "stduent": "없음", "schoolAddress": null }
        ]
```
    - RIGHT JOIN
        - 왼쪽 조건에 상관 없이, 오른쪽 데이를 기준으로 모두 불러 옵니다.
        - 결과
```javascript
        [
            { "stduent": "철수", "schoolAddress": "gangman"},
            { "stduent": "영희", "schoolAddress": "gangman"},
            { "stduent": "태수", "schoolAddress": "gangman"},
            { "stduent": null, "schoolAddress": "sinchon"},
            { "stduent": null, "schoolAddress": null},
            { "stduent": null, "schoolAddress": null}
        ]
```


## INSERT
    - INSERT INTO schools (name, address, type) VALUES ('SchoolOne', 'Gangnam', 'high')


## UPDATE
    - UPDATE schools SET name = 'SchoolTwo' WHERE idx = 2


## DELETE
    - DELETE FROM schools WHERE address = 'Gangnam'


## Query_forms
```javascript
    connection.query('SELECT * FROM `books` WHERE `author` = "David"', function (error, results, fields) {
    // error will be an Error if one occurred during the query
    // results will contain the results of the query
    // fields will contain information about the returned results fields (if any)
    });
    
    connection.query('SELECT * FROM `books` WHERE `author` = ?', ['David'], function (error, results, fields) {
    // error will be an Error if one occurred during the query
    // results will contain the results of the query
    // fields will contain information about the returned results fields (if any)
    });

    connection.query({
        sql: 'SELECT * FROM `books` WHERE `author` = ?',
        timeout: 40000, // 40s
        values: ['David']
    }, function (error, results, fields) {
    // error will be an Error if one occurred during the query
    // results will contain the results of the query
    // fields will contain information about the returned results fields (if any)
    });
```  



## Inection_Attack
    - 외부에서 쿼리문을 서버에 전달하여 데이터베이스를 멋대로 조작하는 공격입니다.
    - 원래 받아야 하는 query
```javascript
    localhost:3000/school?schoolName=school
```
    - 위 코드는 school이라는과 일치할 때의 데이터를 호출합니다.
    - Injection 공격을 아래와 같이 하면 1=1이라는 조건을 넣고, 이는 데이터에서 체크 하는 것이 아니기 때문에 모든 데이터를 가져 옵니다.
```javascript
    localhost:3000/school?schoolName=school' or 1='1
```     
    - 위와 같은 명령은 아래와 같은 쿼리문을 실행시킵니다.
```javascript
    query :  SELECT * FROM TestSchool WHERE name = 'school' or 1='1'
```
    - 결국 모든 데이터를 전부 가져오게 되고, 인젝션 공격이 성공리에 수행 됩니다.




## Escaping_Inection_Attack
```javascript
    // 위 형태에서는 인젝션 공격을 피하기 위해서 다음과 같이 사용해야 합니다.
    var sql = 'SELECT * FROM users WHERE id = ' + connection.escape(userId);

    var post  = {id: 1, title: 'Hello MySQL'};
    var query = connection.query('INSERT INTO posts SET ?', post, function (error, results, fields) {
        if (error) throw error;
        // Neat!
    });

    var userId = 1;
    var columns = ['username', 'email'];
    var query = connection.query('SELECT ?? FROM ?? WHERE id = ?', [columns, 'users', userId], function (error, results, fields) {
        if (error) throw error;
        // ...
    });
```  


## Transaction_And_Rollback
    - 데이터 베이스를 수정하는 것은 상당한 리스크가 있는 일입니다.
    - 때문에 데이터를 수정할 때에는 실제로 DB에 적용하지 않고 취소하는 기능이 필요합니다.
    - 이렇게 데이터를 실제로 수정하지 않고 취소 처리 하는 것을 rollback 이라고 하며,
      rollback 하기 위해서는 transaction을 통해 쿼리 명령어를 실행 해야 합니다.
```javascript
    connection.beginTransaction(function(err) {
        if (err) { throw err; }
        connection.query('INSERT INTO posts SET title=?', title, function (error, results, fields) {
            if (error) {
                return connection.rollback(function() {
                    throw error;
                });
            }
            var log = 'Post ' + results.insertId + ' added';
            connection.query('INSERT INTO log SET data=?', log, function (error, results, fields) {
            if (error) {
                return connection.rollback(function() {
                    throw error;
                });
            }
                connection.commit(function(err) {
                    if (err) {
                    return connection.rollback(function() {
                        throw err;
                    });
                    }
                    console.log('success!');
                });
            });
        });
    });
```  



