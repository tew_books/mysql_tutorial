
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*


- [Cofing](#Cofing)
- [POST](#POST)
- [PUT](#PUT)
- [DELETE](#DELETE)
- [GET](#GET)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## Cofing
    - 설정 파일을 따로 만들어 관리 하도록 합시다.  
```javascript
    module.exports = {
        database: {
            host: 'hostname',
            user: "user",
            password: "password",
            database: "database",
            port: "3306",
            connectionLimit: "10",
            timezone: "utc",
            debug: ['ComQueryPacket', 'RowDataPacket']
        }
    }
```


## POST
```javascript
   router.post('/', function (req, res) {
    const json = req.body;
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
            // Use the connection
        connection.beginTransaction(function(err) {
            if (err) { throw err; }
            var newSchool  = {name: json.name, address: json.address, type:json.type};
            connection.query('INSERT INTO TestSchool SET ?', newSchool, function (error, results, fields) {
                if (error) {
                    return connection.rollback(function() {
                        res.status(200).json({result:false})
                        // throw error;
                    });
                }                   
                connection.commit(function(err) {
                    if (err) {
                    return connection.rollback(function() {
                        res.status(200).json({result:false})
                        // throw err;
                        });
                    }
                    console.log('success!');
                    res.status(200).json({result:true})
                });                
            });
        });
    });
})
```
  

## PUT    
```javascript
router.put('/', function (req, res) {
    const json = req.body;
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
            // Use the connection
        connection.beginTransaction(function(err) {
            if (err) { throw err; }
            var newSchool  = {name: json.name, address: json.address, type:json.type};
            connection.query('UPDATE TestSchool SET ? WHERE idx = ?', [newSchool, json.index], function (error, results, fields) {
                if (error) {
                    return connection.rollback(function() {
                        res.status(200).json({result:false})
                        // throw error;
                    });
                }                   
                connection.commit(function(err) {
                    if (err) {
                    return connection.rollback(function() {
                        res.status(200).json({result:false})
                        // throw err;
                        });
                    }
                    console.log('success!');
                    res.status(200).json({result:true})
                });                
            });
        });
    });
})
```


## DELETE    
```javascript
    const json = req.body;
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
            // Use the connection
        connection.beginTransaction(function(err) {
            if (err) { throw err; }
            connection.query('DELETE FROM TestSchool WHERE idx = ?', json.index, function (error, results, fields) {
                if (error) {
                    return connection.rollback(function() {
                        res.status(200).json({result:false})
                        // throw error;
                    });
                }                   
                connection.commit(function(err) {
                    if (err) {
                    return connection.rollback(function() {
                        res.status(200).json({result:false})
                        // throw err;
                        });
                    }
                    console.log('success!');
                    res.status(200).json({result:true})
                });                
            });
        });
    });
```


## GET    
```javascript
    router.get('/', function (req, res) {
        pool.getConnection(function(err, connection) {
            connection.query('SELECT * FROM TestSchool',  function (error, results, fields) {
                if (error) {
                    res.status(200).json({result:false})
                        // throw error;                
                }                   
                connection.commit(function(err) {
                    if (err) {
                        res.status(200).json({result:false})
                        // throw err;                    
                    }
                    console.log('success!');
                    res.status(200).json({result:results})
                });                
            });
        })    
    })
```

